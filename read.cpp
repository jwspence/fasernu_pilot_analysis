#include<EdbSegP.h>
#include<EdbPattern.h>
#include<EdbDataSet.h>
int main(int argc, char* argv[]) {
	EdbDataProc* dproc = new EdbDataProc(argv[1]);
	dproc->InitVolume(0);
	EdbPVRec* pvr = dproc->GetPVR();
	TFile f(argv[2],"RECREATE");
	TTree *t[29];
	Float_t x,y,z,tx,ty;
	Int_t iz,id;
	for(int i=0;i<29;i++){
		t[i] = new TTree(Form("tree%d",i+90),Form("BTs in layer %d",i+90));
		t[i]->Branch("x",&x,"x/F");
		t[i]->Branch("y",&y,"y/F");
		t[i]->Branch("z",&z,"z/F");
		t[i]->Branch("iz",&iz,"iz/I");
		t[i]->Branch("tx",&tx,"tx/F");
		t[i]->Branch("ty",&ty,"ty/F");
		t[i]->Branch("id",&id,"id/I");
	}
	for (int i = 0, Npatt = pvr->Npatterns(); i < Npatt; i++) {
		EdbPattern* pat = pvr->GetPattern(i);
		for (int j = 0, patN = pat->GetN(); j < patN; j++) {
			EdbSegP* s = pat->GetSegment(j);
			x = s->X();
			y = s->Y();
			z = s->Z();
			iz = s->Plate();
			tx = s->TX();
			ty = s->TY();
			id = s->ID();
			if(j%10000==0)printf("%d %d %lf %lf %lf %d %lf %lf %d\n",i,j,x,y,z,iz,tx,ty,id);
			t[iz-90]->Fill();
		}
	}
	delete pvr;
	for(int i=0;i<29;i++)t[i]->Write();
	f.Close();
	return 0;
}

